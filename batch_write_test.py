import unittest

import numpy as np

import batch_write


class TestStringMethods(unittest.TestCase):

    def test_botofy(self):
        result = batch_write.botofy({
            'str': 'value',
            'int': int(42),
            'float': float(4.2),
            'np.int64': np.array([42], dtype=np.int64)[0],
            'bool': True,
            'None': None,
            'bytes': b'value',
            'list': [
                'value1',
                'value2',
                'value3',
            ],
            'dict': {
                'key': 'value'
            }
        })
        self.assertDictEqual(result, {
            'str': {'S': 'value'},
            'int': {'N': '42'},
            'float': {'N': '4.2'},
            'np.int64': {'N': '42'},
            'bool': {'BOOL': True},
            'None': {'NULL': True},
            'bytes': {'B': b'value'},
            'list': {
                'L': [
                    {'S': 'value1'},
                    {'S': 'value2'},
                    {'S': 'value3'},
                ]
            },
            'dict': {
                'M': {
                    'key': {
                        'S': 'value'
                    }
                }
            }
        })

    # def test_validate_object(self):
    #     s = 'hello world'
    #     self.assertEqual(s.split(), ['hello', 'world'])
    #     # check that s.split fails when the separator is not a string
    #     with self.assertRaises(TypeError):
    #         s.split(2)


if __name__ == '__main__':
    unittest.main()
