import json
import logging
import os
from uuid import uuid4

import boto3
import pandas as pd
from botocore.exceptions import ClientError

logging.basicConfig(
    format='%(asctime)s [%(levelname)s]: %(message)s',
    level=logging.INFO,
)

logger = logging.getLogger(__name__)

TABLE_NAME = 'ExampleData'
BUCKET_NAME = 'thebucketinator'

# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html
dynamodb = boto3.resource('dynamodb')
# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html
s3_client = boto3.client('s3')
# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/quicksight.html
quick_sight = boto3.client('quicksight')


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = f'{str(uuid4())}.{os.path.basename(file_name)}'

    # Upload the file
    try:
        s3_client.upload_file(file_name, bucket, object_name)
        return object_name
    except ClientError as e:
        logger.error('Failed to upload to s3')
        logger.error(e)
    except Exception as e:
        logger.error(e)

    return None


if __name__ == '__main__':
    # fetch data from dynamo
    responses = dynamodb.batch_get_item(
        RequestItems={
            TABLE_NAME: {
                'Keys': [
                    {
                        'Response': 'Yes'
                    },
                    {
                        'Response': 'No'
                    },
                    {
                        'Response': 'Not Sure'
                    },
                ],
                'ConsistentRead': True
            }
        },
        ReturnConsumedCapacity='TOTAL'
    )

    arr = responses['Responses'][TABLE_NAME]
    logger.info(arr)

    # convert data from dynamo to csv
    fn = 'data.csv'
    df = pd.DataFrame(arr)
    df.to_csv(fn)
    logger.info('\n%s', df)

    # upload csv to s3 and retrieve link to it
    objectname = upload_file(file_name=fn, bucket=BUCKET_NAME)
    if objectname is None:
        raise AssertionError('Failed to get object name for s3 object')

    object_link = s3_client.generate_presigned_url(
        'get_object',
        ExpiresIn=0,
        Params={'Bucket': BUCKET_NAME, 'Key': objectname}
    )
    uri = object_link.split('?')[0]
    prefix = uri.replace(objectname, '')

    logger.info('object_link: %s', object_link)
    logger.info('uri: %s', uri)
    logger.info('prefix: %s', prefix)

    manifest = {
        "fileLocations": [
            {
                "URIs": [
                    uri,
                ]
            },
            {
                "URIPrefixes": [
                    prefix,
                ]
            }
        ],
        "globalUploadSettings": {
            "format": "CSV",
            "delimiter": ",",
            "textqualifier": "'",
            "containsHeader": "true"
        }
    }

    logger.info('manifest: %s', manifest)
    manifest = json.dumps(manifest, indent=4)
    with open(f'manifest.{objectname}.json', 'w') as f:
        f.write(manifest)

    # quick sight
    # quick_sight.create_data_set()
