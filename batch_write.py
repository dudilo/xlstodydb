import logging
import sys
from numbers import Number
from time import sleep
from typing import Final, List, Any, Optional

import boto3
import pandas as pd

#  #  START CONFIG  #  #

# os.environ['AWS_ACCESS_KEY_ID'] = ''
# os.environ['AWS_SECRET_ACCESS_KEY'] = ''
# os.environ['AWS_DEFAULT_REGION'] = ''

# Run simulate as true first to see what will be inserted and how much
SIMULATE: Final[bool] = True

# time delay between batch write operations
DRIP_MODE_DELAY: Final[int] = -1  # seconds

# Excel Sheet
XLSX_FILENAME: Final[str] = './data/moviedata.xlsx'
SHEET_NAME: Final[str] = 'Sheet2'

# DynamoDB
TABLE_NAME: Final[str] = 'Movie'
PARTITION_KEY: Final[str] = 'years'
SORT_KEY: Final[Optional[str]] = 'hey'

"""
BatchWriteItem does not support writing more than 25 items at a time:
https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_BatchWriteItem.html
"""
MAX_BATCH_ITEM_WRITE_SIZE: Final[int] = 25  # <=25

# Logging
logging.basicConfig(
    format='%(asctime)s [%(levelname)s]: %(message)s',
    level=logging.INFO,
)

#  #  END CONFIG  #  #

error_flag: bool = False
DRIP_MODE: Final[int] = -1 if not type(DRIP_MODE_DELAY) == int else DRIP_MODE_DELAY
logger = logging.getLogger(__name__)

if MAX_BATCH_ITEM_WRITE_SIZE > 25:
    logger.error('BatchWriteItem does not support writing more than 25 items at a time:')
    logger.error('\thttps://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_BatchWriteItem.html')
    exit(-1)


def main():
    dynamo: Final[Any] = boto3.client('dynamodb')

    logger.info('Reading excel file: %s (sheet: %s)', XLSX_FILENAME, SHEET_NAME)
    sheet = pd.read_excel(XLSX_FILENAME, sheet_name=SHEET_NAME)

    logger.info('Converting to json string')
    json_str = sheet.to_json(orient='index')
    del sheet  # dataframe no longer needed

    logger.info('Parsing json string')
    parsed_json = pd.read_json(json_str)
    del json_str  # json string of sheet no longer needed

    print(parsed_json.keys())

    # divide items into chucks of 25 items
    chunks: List[Any] = []
    n_keys = len(parsed_json[0].keys())
    key = 0
    while key < n_keys:
        obj_arr = []
        count = 0
        while (count < MAX_BATCH_ITEM_WRITE_SIZE) and (key < n_keys):
            obj = botofy(parsed_json[key])
            validate_object(obj)
            obj_arr.append(obj)
            key += 1
            count += 1
        chunks.append(obj_arr)
    del parsed_json  # parsed json string of sheet no longer needed

    # upload chunks/batches
    n_chunks = len(chunks)
    for (idx, chunk) in enumerate(chunks):
        batch_write(dynamo, chunk)
        logger.info(
            f'{idx + 1}/{n_chunks} '
            f'({round((idx + 1)/n_chunks*100)}%) '
            f'batches have been uploaded'
        )
        if DRIP_MODE > 0:
            sleep(DRIP_MODE)

    if SIMULATE:
        logger.warning('NO ITEMS WERE INSERTED; SET SIMULATE TO FALSE')


def validate_object(obj: dict) -> None:
    """
    Object cannot exceed 400 KB
    Object must contain both partition and sort keys
    https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_BatchWriteItem.html
    """
    if type(obj) == dict:
        size = sys.getsizeof(obj) / 1000
        if not size < 400:
            logger.error(f'Row exceeded 400KB')
            raise ValueError('Object cannot exceed max size of 400KB')

        keys = obj.keys()
        if PARTITION_KEY not in keys:
            logger.error(f'"{PARTITION_KEY}" not in {keys}')
            logger.error(f'row must have "{PARTITION_KEY}" member (partition key)')
            raise ValueError('Object does not contain partition key')

        if SORT_KEY is not None and SORT_KEY not in keys:
            logger.error(f'"{SORT_KEY}" not in {keys}')
            logger.error(f'row must have "{SORT_KEY}" member (sort key)')
            raise ValueError('Object does not contain sort key')


def botofy(obj: Any, initial=True) -> Any:
    """
    Request object needs to comply with:
    https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_BatchWriteItem.html
    """
    if obj is None:
        return {
            'NULL': True
        }
    elif isinstance(obj, (str,)):
        return {
            'S': str(obj)
        }
    elif isinstance(obj, (bool,)):
        return {
            'BOOL': bool(obj)
        }
    elif isinstance(obj, Number):
        return {
            'N': str(obj)
        }
    elif isinstance(obj, (bytes,)):
        return {
            'B': bytes(obj)
        }

    if isinstance(obj, (list,)):
        clone = []
        for value in obj:
            clone.append(botofy(value, False))
        return {
            'L': clone,
        }
    botofied = {}
    for i in obj.keys():
        botofied[i] = botofy(obj[i], False)
    return botofied if initial else {
        'M': botofied
    }


def batch_write(dynamo: Any, items: list) -> None:
    """
    Item list cannot contain more than 25 items:
    https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Client.batch_write_item
    """
    if len(items) > MAX_BATCH_ITEM_WRITE_SIZE:
        raise ValueError(f'item list cannot exceed {MAX_BATCH_ITEM_WRITE_SIZE}')

    logger.info('BEGIN TRANSACTION')
    logger.info(f'Writing {len(items)} items to {TABLE_NAME}')
    try:
        request_items = {
            TABLE_NAME: [
                {
                    'PutRequest': {
                        'Item': item,
                    }
                } for item in items
            ]
        }
        result = {'RequestItems': request_items} if SIMULATE else dynamo.batch_write_item(
            RequestItems=request_items,
            ReturnConsumedCapacity='INDEXES',
            ReturnItemCollectionMetrics='SIZE'
        )

        logger.info('END TRANSACTION: %s', result)
    except Exception as e:
        logger.error(e)
        logger.error('TRANSACTION FAILED: Failed to insert rows')
        global error_flag
        error_flag = True


if __name__ == '__main__':
    main()
    if error_flag:
        logger.error('SOME ERRORS OCCURRED!!!')
