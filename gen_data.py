from random import randrange

import pandas as pd

COL_COUNT = 20
ROW_COUNT = 4000
RANGE = range(ROW_COUNT)

data = {}

for i in range(COL_COUNT):
    data[f'{i + 1}.col{i + 1}'] = [randrange(0, 101, 1) for x in RANGE]
df = pd.DataFrame(data)

print(df)
df.to_csv('file4.csv')
