import json
import os
import subprocess
import sys
import tkinter as tk
import tkinter.filedialog as filedialog
import tkinter.messagebox as messagebox
from functools import lru_cache
from numbers import Number
from tkinter import *
from tkinter import ttk
from typing import Final, Iterator, Optional, Dict, Union

COL_FMT = 'I{:>03}'  # placeholder
PAD_COL_NUM = 1
DATA_DIR: Final[str] = os.path.abspath(os.path.normpath(os.path.join('.')))
print(DATA_DIR)

opener = "open" if sys.platform == "darwin" else "xdg-open"
if os.name == 'nt':
    opener = 'start'

class TextEditor:
    def quit_app(self):
        self.root.quit()

    def open_file(self):
        if self.current_item is not None:
            self.text_area.delete(1.0, tk.END)
            filename = self.current_item.path
            content = read_json(filename)
            content = json.dumps(content, indent=4)

            self.text_area.insert(1.0, content)
            self.root.update_idletasks()
            self.old_content = content

    def save_file(self):
        initialdir = os.path.join(DATA_DIR)
        if self.current_item is not None:
            initialdir = self.current_item.parent_folder
        file = filedialog.asksaveasfile(mode='w', initialdir=initialdir)
        if file is not None:
            data = self.text_area.get('1.0', tk.END + '-1c')
            try:
                if os.path.basename(file.name).endswith('.json'):
                    data = json.loads(data)
                    data = json.dumps(data, indent=4)
                file.write(data)
                file.close()
                return True
            except json.JSONDecodeError as e:
                messagebox.showerror(
                    "Invalid JSON",
                    f'Fix issue with file!\n{str(e)}\nReverted to old content'
                )
            except Exception as e:
                messagebox.showerror(
                    "Unknown Error",
                    str(e)
                )
            file.write(self.old_content)
            file.close()
            return False

    def format_text_area(self):
        data = self.text_area.get('1.0', tk.END + '-1c')
        try:
            data = json.loads(data)
            data = json.dumps(data, indent=4)
            self.text_area.delete(1.0, tk.END)
            self.text_area.insert(1.0, data)
            return True
        except json.JSONDecodeError as e:
            messagebox.showerror(
                "Invalid JSON",
                f'Failed to parse JSON.\n\nFix: {str(e)}'
            )
        except Exception as e:
            messagebox.showerror(
                "Unknown Error",
                str(e)
            )
        return False

    def open_folder(self):
        subprocess.call([opener, self.current_item.parent_folder])

    def __init__(self, root, _current_item):
        self.root = root
        self.old_content = ''
        self.current_item = _current_item
        self.root.title(_current_item.filename)
        self.root.geometry('600x550')
        frame = tk.Frame(self.root, width=600, height=550)
        scrollbar = tk.Scrollbar(frame)
        self.text_area = tk.Text(
            frame, width=600, height=550,
            yscrollcommand=scrollbar.set,
            padx=10, pady=10)
        scrollbar.config(command=self.text_area.yview)
        scrollbar.pack(side='right', fill='y')
        self.text_area.pack(fill='both', expand=True)
        frame.pack()

        the_menu = tk.Menu(self.root)
        file_menu = tk.Menu(the_menu, tearoff=0)
        file_menu.add_command(label='Save', command=self.save_file)
        file_menu.add_command(label='Format JSON', command=self.format_text_area)
        file_menu.add_command(label='Open Folder', command=self.open_folder)
        file_menu.add_separator()
        file_menu.add_command(label='Quit', command=self.quit_app)
        the_menu.add_cascade(label='File', menu=file_menu)
        self.root.config(menu=the_menu)
        self.open_file()


class JsonTreeView:
    def quit_app(self):
        self.root.quit()

    def tree_view_ify(self, obj: dict, parent_id):
        if obj is None:
            self.tree.insert(parent_id, END, text=f'{json.dumps(obj)}')
            return None
        elif isinstance(obj, (str,)):
            self.tree.insert(parent_id, END, text=f'{obj if obj else "<No Value>"} (string)')
            return obj
        elif isinstance(obj, (bool,)):
            self.tree.insert(parent_id, END, text=f'{json.dumps(obj)} (boolean)')
            return obj
        elif isinstance(obj, Number):
            self.tree.insert(parent_id, END, text=f'{obj} (number)')
            return obj
        elif isinstance(obj, (bytes,)):
            self.tree.insert(parent_id, END, text=f'{str(obj)} (bytes)')
            return bytes(obj)

        if isinstance(obj, (list,)):
            clone = []
            for value in obj:
                clone.append(self.tree_view_ify(value, parent_id))

            if len(clone) == 0:
                self.tree.insert(parent_id, END, text=f'[Empty Array]')
            return clone

        tree_view_part = {}
        for _key in obj.keys():
            key_id = self.tree.insert(parent_id, END, text=_key)
            tree_view_part[_key] = self.tree_view_ify(obj[_key], key_id)
        return tree_view_part

    def __init__(self, root, title: str, item_to_show: dict):
        self.root = root
        self.show = item_to_show
        self.title = title
        self.root.title(self.title)
        self.root.geometry('600x550')
        frame = tk.Frame(self.root, width=600, height=550)
        scrollbar = tk.Scrollbar(frame)
        self.tree = ttk.Treeview(
            self.root, height=550,
            show='tree headings',
            yscrollcommand=scrollbar.set,
        )
        self.tree['columns'] = ('view',)
        self.tree.column('view', width=600, stretch=True, anchor='ne')
        self.tree.heading('view', text='')

        scrollbar.config(command=self.tree.yview)
        self.tree.pack(side='right', fill='y')
        frame.pack()

        self.top_id = self.tree.insert('', END, text=self.title)
        self.tree_view_ify(item_to_show, self.top_id)
        the_menu = tk.Menu(self.root)
        file_menu = tk.Menu(the_menu, tearoff=0)
        file_menu.add_command(label='Quit', command=self.quit_app)
        the_menu.add_cascade(label='File', menu=file_menu)
        self.root.config(menu=the_menu)


class FolderItem:
    @staticmethod
    def convert_bytes(num):
        step_unit = 1000.0  # 1024 bad the size

        for x in ['B', 'KB', 'MB', 'GB', 'TB']:
            if num < step_unit:
                return "%3.1f %s" % (num, x)
            num /= step_unit

    def __init__(self, path):
        self.path = path
        self.filename = os.path.basename(self.path)
        self.parent_folder = os.path.normpath(self.path.replace(self.filename, ''))
        self.parent_folder_name = self.path.split(os.path.sep)[-2]
        self.size = os.path.getsize(self.path)
        self.size_str = FolderItem.convert_bytes(self.size)

    def __str__(self):
        return f'{self.parent_folder}{os.path.sep}{self.filename}'


def get_selected():
    global current_item
    _key: Optional[str] = None
    if len(tree.selection()) > 0:
        _key = tree.selection()[0]
        if _key in items:
            selected: FolderItem = items[_key]
            if isinstance(selected, (str,)):
                _key = selected
                selected = items[_key]
            current_item = selected
            return selected
    current_item = None
    return _key


def show_selected() -> None:
    selected = get_selected()
    if isinstance(selected, (FolderItem,)):
        print(selected)
        open_viewer_window()
    else:
        print(selected, 'not in items')


def edit_selected() -> None:
    selected = get_selected()
    if isinstance(selected, (FolderItem,)):
        open_editor_window()
    else:
        print(selected, 'not in items')


def open_selected() -> None:
    selected = get_selected()
    if isinstance(selected, (FolderItem,)):
        if os.name == 'nt':
            os.startfile(selected.parent_folder)
        else:
            subprocess.call([opener, selected.parent_folder])
    else:
        print(selected, 'not in items')


def dirs() -> Iterator[str]:
    for (parent, _, filenames) in os.walk(DATA_DIR):
        for filename in filenames:
            if filename.endswith('.json'):
                yield os.path.join(parent, filename)


@lru_cache(maxsize=4)
def read_json(path: str) -> dict:
    messages = []
    try:
        with open(path, 'rb') as f:
            return json.load(f)
    except Exception as e1:
        messages.append(e1)
        try:
            with open(path, 'r', encoding='utf-8') as f:
                return json.load(f)
        except Exception as e2:
            print('e2', e2)
            messages.append(e2)
    return {'messages': messages}


def open_editor_window():
    root = Toplevel(ws)
    TextEditor(root, _current_item=current_item)


def open_viewer_window():
    root = Toplevel(ws)
    JsonTreeView(
        root,
        title=current_item.path,
        item_to_show=read_json(current_item.path)
    )


current_item: Optional[FolderItem] = None
items: Dict[str, Union[FolderItem, str]] = {}
for (idx, directory) in enumerate(dirs()):
    key = COL_FMT.format((idx + 1) + PAD_COL_NUM)
    items[key] = FolderItem(directory)

ws = Tk()
tree = ttk.Treeview(ws, height=20, show='tree headings')
tree['columns'] = ('view',)
tree.column('view', width=300, stretch=True, anchor='ne')
tree.heading('view', text='')

tree.pack()

files_id = tree.insert('', END, text=f'JSON Files ({len(items.keys())})')
tree.item(files_id, open=True)
new_items: Dict[str, Union[FolderItem, str]] = {}
n = '{:>0%s}' % (len(str(len(items.keys()))),)
for (idx, key) in enumerate(sorted(set(items.keys()))):
    i = items[key]
    if isinstance(i, (FolderItem,)):  # to suppress linting error (should always be true)
        number = n.format(idx + 1)
        video_id = tree.insert(files_id, END, text=f"{number}. {str(i)} ({i.size_str})")
        new_items[video_id] = i
    else:
        assert False, 'Unreachable'
items = new_items.copy()
new_items.clear()

Button(ws, text="Show Tree View", command=show_selected).pack()
Button(ws, text="Edit Data", command=edit_selected).pack()
Button(ws, text="Open Folder", command=open_selected).pack()

style = ttk.Style()
ws.mainloop()
