import os
from time import time

import psutil


def process_memory():
    process = psutil.Process(os.getpid())
    mem_info = process.memory_info()
    return mem_info.rss


def perf(f):
    def wrap(*args):
        mem_before = process_memory()
        start_time = time()
        ret = f(*args)
        end_time = time()
        mem_after = process_memory()
        print(
            ('[PERF] %s(...) function took %0.3f µs (%s)\n' % (
                f.__name__,
                (end_time - start_time) * 1000.0,
                'consumed memory: {:,} bytes'.format(
                    mem_before, mem_after, mem_after - mem_before
                )

            )).strip()
        )
        return ret

    return wrap
